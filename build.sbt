import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

name := "play-diagnostics"

organization := "uk.co.unclealex"
organizationName := "unclealex"

Global / onChangedBuildSource := ReloadOnSourceChanges

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  Resolver.jcenterRepo)

scalaVersion := "2.13.3"

scalacOptions in Test ++= Seq("-Yrangepos")

lazy val metricsDependencies = Seq("graphite", "healthchecks", "json", "jvm", "logback").map { dep =>
  "io.dropwizard.metrics" % s"metrics-$dep" % "4.0.3"
}

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "filters-helpers" % "2.8.2",
  "com.typesafe.play" %% "play" % "2.8.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % "test")

libraryDependencies ++= metricsDependencies

fork in Test := false

organizationHomepage := Some(url("https://bitbucket.org/UncleAlex72/"))

scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/UncleAlex72/play-diagnostics"),
    "scm:git@bitbucket.org:UncleAlex72/play-diagnostics.git"
  )
)
developers := List(
  Developer(
    id = "1",
    name = "Alex Jones",
    email = "alex.jones@unclealex.co.uk",
    url = url("https://bitbucket.org/UncleAlex72/")
  )
)

description := "Diagnostics for Play applications"

licenses := List(
  "Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
homepage := Some(url("https://bitbucket.org/UncleAlex72/scalajs-redux"))

// Remove all additional repository other than Maven Central from POM
pomIncludeRepository := { _ =>
  false
}

publishTo := sonatypePublishToBundle.value

publishMavenStyle := true

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("publishLocal"),
  releaseStepCommand("publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
