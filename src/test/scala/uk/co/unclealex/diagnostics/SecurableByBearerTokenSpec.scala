package uk.co.unclealex.diagnostics

import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout
import org.scalatest.{Assertion, AsyncFlatSpec, Matchers, OneInstancePerTest}
import play.api.http.{Status, Writeable}
import play.api.mvc._
import play.api.test.{FakeRequest, Helpers}

import scala.concurrent.Future

class SecurableByBearerTokenSpec extends AsyncFlatSpec with Matchers with OneInstancePerTest {

  behavior of "SecurableByBearerToken"

  val GOOD_TOKEN = "good token"
  val BAD_TOKEN = "bad token"

  it should "mark a request without a bearer token as unauthorised" in {
    executeTest(None, 0, Status.UNAUTHORIZED)
  }

  it should "reject a request with an invalid a bearer token as forbidden" in {
    executeTest(Some(BAD_TOKEN), 0, Status.FORBIDDEN)
  }

  it should "execute a request with an the correct bearer token" in {
    executeTest(Some(GOOD_TOKEN), 1, Status.OK)
  }

  def executeTest(
                   maybeToken: Option[String],
                   expectedActionExecutionCount: Int,
                   expectedStatusCode: Int): Future[Assertion] = {
    implicit val sys: ActorSystem = ActorSystem("MyTest")
    implicit val mat: ActorMaterializer = ActorMaterializer()
    implicit val timeout: Timeout = Helpers.defaultAwaitTimeout
    implicit val w: Writeable[AnyContentAsEmpty.type] = Helpers.writeableOf_AnyContentAsEmpty
    val controllerComponents: ControllerComponents =
      Helpers.stubControllerComponents(playBodyParsers = Helpers.stubPlayBodyParsers(mat))
    val controller = new SecuredController(controllerComponents)
    val request: FakeRequest[AnyContentAsEmpty.type] = maybeToken.foldLeft(FakeRequest()) { (request, token) =>
      request.withHeaders("Authorization" -> ("Bearer " + token))
    }
    val eventualResult: Future[Result] = Helpers.call(controller.action, request)
    eventualResult.map { result =>
      result.header.status should equal(expectedStatusCode)
      controller.actionExecutionCount.get() should equal(expectedActionExecutionCount)
    }
  }

  class SecuredController(controllerComponents: ControllerComponents)
    extends AbstractController(controllerComponents) with SecurableByBearerToken {

    val bearerToken: String = GOOD_TOKEN

    val actionExecutionCount = new AtomicInteger(0)
    def action = SecuredByBearerToken {
      Action { implicit request =>
        actionExecutionCount.incrementAndGet()
        Ok("Done")
      }
    }
  }

}

