package uk.co.unclealex.diagnostics

import com.kenshoo.play.metrics.MetricsController
import play.api.mvc._

/**
  * A wrapper for a [[MetricsController]] that secures any metrics request.
  */
class SecureMetricsController(metricsController: MetricsController,
                              val bearerToken: String,
                              override val controllerComponents: ControllerComponents)
  extends AbstractController(controllerComponents) with SecurableByBearerToken {

  def metrics: Action[AnyContent] = SecuredByBearerToken(metricsController.metrics)
}
