package uk.co.unclealex.diagnostics

import play.api.mvc._

import scala.concurrent.Future

/**
  * A trait that allows web methods to be secured by a bearer token.
  */
trait SecurableByBearerToken extends BaseController {

  /**
    * The value required to be the bearer token.
    */
  val bearerToken: String

  private lazy val requiredAuthorizationValue: String = "Bearer " + bearerToken

  /**
    * Action composition that allows an action to be secured using a bearer token.
    * @param action The action to wrap.
    * @tparam A
    */
  def SecuredByBearerToken[A](action: Action[A]): Action[A] = Action.async(action.parser) { request =>
    request.headers.get("Authorization") match {
      case None =>
        Future.successful(Unauthorized("unauthorized"))
      case Some(headerValue) =>
        if (headerValue == requiredAuthorizationValue) {
          action(request)
        }
        else {
          Future.successful(Forbidden("forbidden"))
        }
    }
  }
}
