package uk.co.unclealex.diagnostics

import java.util.{SortedMap => JavaSortedMap}

import com.codahale.metrics.health.{HealthCheck, HealthCheckRegistry}
import com.codahale.metrics.json.HealthCheckModule
import com.fasterxml.jackson.databind.ObjectMapper
import play.api.mvc._

import scala.collection.JavaConverters._

/**
  * A simple controller that performs a health-check when called..
  */
class HealthCheckController(healthCheckRegistry: HealthCheckRegistry,
                            val bearerToken: String,
                            override val controllerComponents: ControllerComponents)
  extends AbstractController(controllerComponents) with SecurableByBearerToken {

  val objectMapper = new ObjectMapper()
  objectMapper.registerModule(new HealthCheckModule())

  def healthCheck: Action[AnyContent] = SecuredByBearerToken {
    Action { implicit request =>
      val healthCheckResults: JavaSortedMap[String, HealthCheck.Result] = healthCheckRegistry.runHealthChecks()
      val healthCheckJson: String = objectMapper.writeValueAsString(healthCheckResults)
      val isHealthy: Boolean = healthCheckResults.values().asScala.forall(_.isHealthy)
      val status: Int = if (isHealthy) OK else INTERNAL_SERVER_ERROR
      Status(status)(healthCheckJson)
    }
  }
}
