package uk.co.unclealex.diagnostics

import java.time.{Clock, Duration, Instant}

import akka.stream.Materializer
import play.api.Logger
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

/**
  * A class that logs requests made to the server.
  */
class AccessLoggingFilter(val clock: Clock)(implicit val mat: Materializer, ec: ExecutionContext) extends Filter {

  val logger = Logger("access")

  def apply(nextFilter: RequestHeader => Future[Result])
           (requestHeader: RequestHeader): Future[Result] = {

    val loggerHexId: String = requestHeader.id.toHexString
    val headers: String = requestHeader.headers.toMap.toSeq.flatMap { kvs =>
      val (k, vs) = kvs
      vs.map(v => s"$k: $v")
    }.mkString(", ")
    val uri: String = requestHeader.uri
    val method: String = requestHeader.method
    val version: String = requestHeader.version
    val remoteAddress: String = requestHeader.remoteAddress
    val startTime: Instant = clock.instant()

    logger.info(s"[$loggerHexId] [$remoteAddress] [$version] [$method] $uri > $headers")
    nextFilter(requestHeader).map { result =>
      val duration: Long = Duration.between(startTime, clock.instant()).toMillis
      val responseSize: String =  result.body.contentLength.map(_.toString).getOrElse("-")
      logger.info(s"[$loggerHexId] [$method] $uri ${duration}ms ${result.header.status} $responseSize")
      result
    }
  }
}
