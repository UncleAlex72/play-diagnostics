package uk.co.unclealex

import play.api.http.{ContentTypeOf, ContentTypes}
import play.api.mvc.Codec

/**
  * Implicits for custom Twirl templates.
  */
package object templates {

  implicit def contentTypeJson(implicit codec: Codec): ContentTypeOf[Json] =
    ContentTypeOf[Json](Some(ContentTypes.JSON))

}
