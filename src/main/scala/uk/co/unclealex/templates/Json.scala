package uk.co.unclealex.templates

import play.mvc.Http.MimeTypes
import play.twirl.api.{BufferedContent, Format}

import scala.collection.immutable

/**
  * JSON twirl templates.
  * @param elements
  * @param text
  */
class Json(elements: immutable.Seq[Json] = immutable.Seq.empty[Json], text: String = "") extends BufferedContent[Json](elements, text) {

  override def contentType: String = MimeTypes.JSON
}

object Json {
  def apply(elements: immutable.Seq[Json]): Json = new Json(elements = elements)
  def apply(text: String): Json = new Json(text = text)
  def apply(): Json = new Json()
}

/**
  * A simple format for JSON templates that does not do any escaping.
  */
object JsonFormat extends Format[Json] {

  def raw(text: String): Json = Json(text)
  def escape(text: String): Json = Json(text)

  override def empty: Json = Json()

  override def fill(elements: immutable.Seq[Json]): Json = Json(elements)
}